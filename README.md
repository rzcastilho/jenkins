[![](https://images.microbadger.com/badges/image/rodrigozc/jenkins.svg)](https://microbadger.com/images/rodrigozc/jenkins "Get your own image badge on microbadger.com")

[![](https://images.microbadger.com/badges/version/rodrigozc/jenkins.svg)](https://microbadger.com/images/rodrigozc/jenkins "Get your own version badge on microbadger.com")

# Jenkins + Docker + AWS Client + GCloud SDK + kubectl

Jenkins with Docker, AWS Client, GCloud SDK and kubectl to automate build images and service updates.

## Image setup

#### AWS

TODO

#### GCloud

TODO

#### kubectl

TODO
