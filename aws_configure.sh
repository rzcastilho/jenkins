#!/bin/sh

if [ ! -e ~/.aws ]; then
  aws configure set aws_access_key_id $AWS_DEFAULT_ACCESS_KEY_ID
  aws configure set aws_secret_access_key $AWS_DEFAULT_SECRET_ACCESS_KEY
  aws configure set default.region $AWS_DEFAULT_REGION
  aws configure set default.region $AWS_DEFAULT_OUTPUT
fi
