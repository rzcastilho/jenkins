#!/bin/sh

echo "Login to amazon repository..."
DOCKER_LOGIN=`aws ecr get-login --no-include-email`
echo "  -> $DOCKER_LOGIN"
sh -c "$DOCKER_LOGIN"

echo "Login to gcloud repository..."
GCLOUD_JSON=`cat $GCLOUD_KEY_FILE`
DOCKER_LOGIN="docker login -u _json_key -p '$GCLOUD_JSON' https://gcr.io"
echo "  -> $DOCKER_LOGIN"
sh -c "$DOCKER_LOGIN"
